#! /usr/bin/env node

import { purge } from '../src/dictionnary.js';

const words = process.argv.slice(2);

await Promise.all(
	words
		.map((s) => s.toUpperCase())
		.map((s) => purge(s))
);
