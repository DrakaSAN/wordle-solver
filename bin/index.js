#! /usr/bin/env node

import process from 'process';

const script = process.argv.splice(2, 1)[0];

if (script === 'solver') {
	await import('./solver.js');
}

if (script === 'purge') {
	await import('./purge.js');
}

if (script === 'hint') {
	await import('./hint.js');
}
