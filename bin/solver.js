#! /usr/bin/env node

import { start } from '../src/guesser.js';
import '../src/input/type.js';

/** @type {InputModule} */
let inputModule;

if (process.argv.includes('--interactive')) {
	inputModule = await import('../src/input/readline.js');
} else {
	const module = await import('../src/input/preloaded.js');
	const preload = module.default;
	inputModule = preload([
		'?____',
		'_O_OO'
	]);
}

await start(inputModule);
