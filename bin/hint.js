#! /usr/bin/env node

import minimist from 'minimist';
import dictionnary from '../src/dictionnary.js';
import { filter } from '../src/match.js';

const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
const dict = await dictionnary.load();

const args = minimist(
	process.argv.slice(2),
	{ alias: {
		c: 'contains',
		e: 'excluded',
		w: 'word'
	} }
);

function init(arg) {
	if (Array.isArray(arg)) {
		arg = arg[0];
	}
	if (!arg) {
		arg = [];
	}
	if (!Array.isArray(arg)) {
		arg = arg.split('');
	}

	return arg;
}

console.log('args', args);

/** @type {import('../src/guesser').State} */
const state = {
	contains: init(args.contains?.toUpperCase()),
	excluded: init(args.excluded?.toUpperCase()),
	known: init(args._)
		.map((l) => l.toUpperCase())
		.map((l) => alphabet.includes(l) ? l : undefined)
};

console.log('state', state);
console.log(filter(args.w || dict, state).join('\n'));
