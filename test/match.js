import { describe, it } from 'mocha';
import { expect } from 'chai';
import { match } from '../src/match.js';

describe('match', () => {
	describe('match', () => {
		it('returns true on exact match', () => {
			expect(
				match('TEST', { known: ['T', 'E', 'S', 'T'] })
			).to.equal(true);
		});

		it('returns true on partial match', () => {
			expect(
				match('TEST', { known: ['T', 'E', 'S', undefined] })
			).to.equal(true);
		});

		it('returns false on partial mismatch', () => {
			expect(
				match('TEST', { known: ['T', 'A', 'S', 'T'] })
			).to.equal(false);
		});

		it('returns false on full mismatch', () => {
			expect(
				match('TEST', { known: ['N', 'O', 'P', 'E'] })
			).to.equal(false);
		});

		it('', () => {
			expect(
				match('DIDDY', { known: [undefined, 'I', undefined, 'D', 'Y'] })
			).to.equal(true);
		});

		it('', () => {
			expect(
				match('TEDDY', { known: [undefined, 'I', undefined, 'D', 'Y'] })
			).to.equal(false);
		});
	});
});
