import { writeFile } from 'fs/promises';

/**
 * @typedef {Array<string, undefined>} Pattern
 */

/**
 * @typedef State
 * @property {Array<string>} contains
 * @property {Pattern} known
 * @property {Array<string>} excluded
 */

/**
 * @param {object} arg
 * @param {State} arg.state
 * @param {string[]} arg.wordList
 * @param {number} turn
 */
export async function save({ state, wordList }, turn) {
	await Promise.all([
		writeFile(new URL(`../logs/${turn}-wordlist.txt`, import.meta.url), wordList.join('\n')),
		writeFile(new URL(`../logs/${turn}-state.json`, import.meta.url), JSON.stringify(state, undefined, '\t'))
	]);
}

/**
 * @param {number} turn
 * @param {State} state
 * @param {string[]} wordList
 */
export function print(state, wordList) {
	console.debug(JSON.stringify(state));
	console.debug(wordList.join(' '));
}

/**
 * @param {string} string
 * @returns {Array<{ state: State, wordList: string[] }>}
 */
export function parse(string) {
	return string.split('\n').reduce((acc, cur, i) => {
		if (i % 2 === 0) {
			acc.push({ state: JSON.parse(cur) });
		} else {
			acc[acc.length - 1].wordList = cur.split(' ');
		}
	}, []);
}

export default {
	save,
	print
};
