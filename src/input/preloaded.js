import './type.js';

/** @type {(string[]) => InputModule} */
export default (lines) => {
	let turn = 0;

	async function init() {
	}

	/**
	 * @param {string} guess
	 * @returns {string}
	*/
	// eslint-disable-next-line no-unused-vars
	async function validate(guess) {
		console.log(`Result for ${guess.value} ?`);
		const line = lines[turn];
		turn += 1;
		console.log(line);
		return line;
	}

	async function close() {
	}

	return {
		init,
		validate,
		close
	};
};
