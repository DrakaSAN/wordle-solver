/**
 * @typedef InputModule
 * @property {() => Promise<void>} init
 * @property {(string) => Promise<string>} validate
 * @property {() => Promise<void>} close
 */
