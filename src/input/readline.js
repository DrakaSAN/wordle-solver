import readline from 'readline/promises';
import './type.js';

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
});

export async function init() {
}

export async function validate(guess) {
	return rl.question(`Result for ${guess.value} ?\n`);
}

export async function close() {
	rl.close();
}

/** @type {InputModule} */
export default {
	init,
	validate,
	close
};
