import { readFile, writeFile } from 'fs/promises';

const location = new URL('../resources/words.txt', import.meta.url);

/**
 * @param {string[]} wordList
 */
export async function save(wordList) {
	await writeFile(location, wordList.join('\n'), { encoding: 'utf-8' });
}

/**
 * @returns {Promise<string[]>}
 */
export async function load() {
	const file = await readFile(location, { encoding: 'utf-8' });
	return file.split('\n');
}

/**
 * @param {string} word
 */
export async function purge(word) {
	const wordList = await load();
	const wordPosition = wordList.indexOf(word);
	if (wordPosition === -1) {
		throw new Error(`Can't purge word ${word}, it doesn't exist in the wordlist`);
	}
	wordList.splice(wordPosition, 1);
	await save(wordList);
}

export default {
	load,
	purge
};
