import dictionnary from './dictionnary.js';
import { filter } from './match.js';
import { print as printState } from './state.js';
import './input/type.js';

/**
 * @typedef {Array<'_' | '?' | 'O'>} Response
 */

/**
 * @typedef {import('./state.js').State} State
 */

/**
 * @param {Response} response
 * @param {string} guess
 * @param {State} state
 * @returns {State}
 */
export function applyResponse(response, guess, state) {
	/**
	 * @type {State}
	 */
	const nextState = {
		contains: [...state.contains],
		known: [],
		excluded: [...state.excluded]
	};

	response.forEach((r, i) => {
		const letter = guess[i];
		if (r === 'O') {
			nextState.contains.push(letter);
			nextState.known.push(letter);
		} else if (r === '?') {
			nextState.contains.push(letter);
			nextState.known.push(undefined);
		} else {
			// r === '_'
			if (!nextState.contains.includes(letter)) {
				nextState.excluded.push(letter);
			}
			nextState.known.push(undefined);
		}
	});

	return nextState;
}

/**
 * @param {string} string
 * @returns {boolean}
 */
export function isResponse(string) {
	return (
		// Standard response
		(string.length === 5 && string.split('').every((letter) => '_?O'.includes(letter)))
		|| 'X' || '>'
	);
}

/**
 * @param {string[]} dict
 */
export function* guesserGenerator(dict) {
	let wordList = [...dict];

	/**
	 * @type {State}
	 */
	let state = {
		contains: [],
		known: [],
		excluded: []
	};

	/**
	 * @type {string}
	 */
	let guess = 'DEALT';
	// Best opening in hard mode
	// (https://www.forbes.com/sites/erikkain/2022/08/19/the-updated-wordle-bot-now-has-a-new-best-starting-word/)
	while (wordList.length > 0) {
		const line = yield guess;
		if (!isResponse(line)) {
			console.error('Response should be 5 letters and composed of _, ? or O');
			console.error('_ for grey letters (letter is not in the word)');
			console.error('? for yellow letters (letter in teh word but in the wrong place');
			console.error('O for green letters (right letter at the right place)');
			console.error();
			console.error('You can also use X to tell the word is invalid');
			console.error('Or > to skip that word');
			continue;
		}
		if (line === 'X') {
			dictionnary.purge(guess);
			wordList.splice(0, 1);
			continue;
		}
		if (line === '>') {
			wordList.push(wordList.splice(0, 1));
			guess = wordList[0];
			continue;
		}
		if (line === '<') {
			wordList.unshift(wordList.splice(-1, 1));
			guess = wordList[0];
			continue;
		}

		const response = line.split('');

		if (response.every((letter) => letter === 'O')) {
			break;
		}

		state = applyResponse(response, guess, state);

		wordList = filter(wordList, state);
		guess = wordList[0];
		printState(state, wordList);
	}
}

/**
 * @param {InputModule} inputModule
 */
export async function start(inputModule) {
	await inputModule.init();
	const dict = await dictionnary.load();
	const guesser = guesserGenerator(dict);

	let guess = guesser.next();

	while (!guess.done) {
		const line = await inputModule.validate(guess);
		guess = guesser.next(line);
	}

	await inputModule.close();
}
