import './guesser.js';

/**
 * @param {string[]} wordList
 * @param {import('./guesser.js').State} state
 * @returns {boolean}
 */
export function filter(wordList, state) {
	return wordList.filter((word) => {
		const forbiddenLetter = state.excluded.find((letter) => word.includes(letter));
		if (forbiddenLetter) { return false; }

		const missingLetter = state.contains.find((letter) => !word.includes(letter));
		if (missingLetter) { return false; }

		const patternMatch = match(word, state);
		if (!patternMatch) { return false; }

		return true;
	});
}

/**
 * @param {import('./guesser.js').State} state
 * @param {string} word
 * @returns {boolean}
 */
export function match(word, state) {
	return !state.known.find((letter, i) => (
		letter
		&& word[i] !== letter
	));
}
