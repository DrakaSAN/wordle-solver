import { readFile, writeFile } from 'fs/promises';

const originalWordList = await readFile(
	new URL('../resources/words_alpha.txt', import.meta.url),
	{ encoding: 'utf-8' }
);

const fiveLetterWordList = originalWordList
	.split('\n')
	.map((line) => line.trim().toUpperCase())
	.filter((word) => word.length === 5)
	.join('\n');

await writeFile(
	new URL('../resources/words.txt', import.meta.url), fiveLetterWordList,
	{ encoding: 'utf-8' }
);
